import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ProductsData } from '../../providers/ProductMetaData';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';
import { ProductPage } from '../product/product';

/**
 * Generated class for the SearchModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search-modal',
  templateUrl: 'search-modal.html',
})
export class SearchModalPage {

  @ViewChild('input') searchInput;

  search_key: string = "";
  results: ProductsData [] = [];
  busy = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public woo: WoocommerceProvider,
    public viewCtrl: ViewController,
    ) {
      if(this.search_key = "") {
        this.results.length = 0;
      }
  } 

  getProducts(clear = false) {
    if (clear) {
      this.results.length = 0;
      this.busy = true;
    }
    if(this.search_key == "") {
      this.busy = false;
      this.reset();
    }
    else {
      this.results =this.woo.searchProducts(this.search_key);
    }
    console.log(this.results);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchModalPage');
  }
  reset(){
		this.results.length = 0;
  }
  dismiss() {
		this.viewCtrl.dismiss();
  }
  goToProduct(id) {
    this.navCtrl.push(ProductPage, { product: id, } );
  }

}
