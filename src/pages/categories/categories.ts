import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ProductsPage } from '../products/products';
import { CategoryData } from '../../providers/model';
import 'rxjs/add/operator/toPromise';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';



@IonicPage()
@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {

  categoires:CategoryData [] = [];
  aloneCate: CategoryData [] = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public woo: WoocommerceProvider, 
    public load: LoadingController ) {
  }
  ionViewWillEnter() {
    this.getItems();
    this.woo.getUserCart();
  }
  ionViewDidLoad() {
    this.presentLoadingDefault();

  }
  goProducts(id: number) {
     this.navCtrl.push(ProductsPage, { category: id });
  }
  presentLoadingDefault() {
    let loader = this.load.create({
      spinner: "bubbles",
      content: "جاري التحميل",
      duration: 2000
    });

    loader.present();

    setTimeout(() => {
      this.getItems();
    }, 2000);
  }
  getItems() {
    this.woo.getCate().forEach(element => {
      if(this.categoires.length <= 5) {
        switch(element.id) {
          case 174:
            this.categoires.push(element);
            break;
          case 112:
            this.categoires.push(element);
            break;
          case 229:
            this.categoires.push(element);
            break;
          case 110:
            this.aloneCate.push(element);
            break;
          case 175:
            this.categoires.push(element);
            break;
          case 124:
            this.categoires.push(element);
            break;
          case 217:
            this.categoires.push(element);
            break;
          default:  
            break;
        }
      }
    });
  }

 
}
