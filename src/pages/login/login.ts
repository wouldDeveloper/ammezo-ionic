import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController  } from 'ionic-angular';
import { RegisterPage } from '../register/register'
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { response } from '../../providers/users';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  login_form: FormGroup;
  error_message: string;
 
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loading: LoadingController,
    public formBuilder: FormBuilder,
    public auth: AuthenticationProvider,
    ) {
      this.login_form = this.formBuilder.group({
        username: new FormControl('', Validators.compose([
          Validators.required
        ])),
        password: new FormControl('', Validators.required)
      });
    }
  
 

  ionViewDidLoad() {
    
  }

  login(value){
    let loading = this.loading.create();
    loading.present();
    console.log(value.username);
    this.auth.doLogin(value.username, value.password)
    .subscribe((res: response) => {
      this.auth.setUser({
        token: res.token,
        username: value.username,
        displayname: res.user_display_name,
        email: res.user_email
      });
       loading.dismiss();
       this.navCtrl.setRoot(TabsPage);
     },
     err => {
       loading.dismiss();
       this.error_message = "Invalid credentials. Try with username 'aa' password 'aa'.";
       console.log(err);
     })
  }

  skipLogin(){
    this.navCtrl.setRoot(HomePage);
  }
  register() {
    this.navCtrl.push(RegisterPage);
  }
}
