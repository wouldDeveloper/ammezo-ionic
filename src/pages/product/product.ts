import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { ProductsData, Image, cart } from '../../providers/ProductMetaData';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';
import { CartProvider } from '../../providers/cart/cart';
import { FavouritesProvider } from '../../providers/favourites/favourites';


@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {

  product: ProductsData = <ProductsData>{};
  img: Image = <Image>{};

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public woo: WoocommerceProvider, 
    public cart: CartProvider, 
    public load: LoadingController,
    public favs: FavouritesProvider,
    public toast: ToastController ) {
  }

  ionViewWillEnter() {
    this.getItems();
    this.getImg();
    // console.log(this.cart.getCart());
  }

  ionViewWillLeave() {
    this.product  = <ProductsData>{};
    this.img = <Image>{};
  }

  presentLoadingDefault() {
    let loader = this.load.create({
      spinner: "bubbles",
      content: "جاري التحميل",
      duration: 4000
    });

    loader.present();

    setTimeout(() => {
      this.getItems();
      this.getImg();
    }, 4000);
  }

  ionViewDidLoad() {
    this.presentLoadingDefault();
    console.log(this.navParams.get('product'));
  }

  getItems() {
    this.product = this.woo.getProduct(this.navParams.get('product'));
  }
  getImg() {
    this.img = this.woo.getImg(this.navParams.get("product"));
  }
  presentToast() {
    let toast = this.toast.create({
      message: 'تمت الإضافة المنتج  !',
      duration: 3000,
      position: 'top',
      cssClass: "rtl"
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  itemInCart() {
    let toast = this.toast.create({
      message: ' المنتج موجود داخل السلة !',
      duration: 3000,
      position: 'top',
      cssClass: 'rtl'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  itemInFav() {
    let toast = this.toast.create({
      message: ' المنتج موجود في المفضلة !',
      duration: 3000,
      position: 'top',
      cssClass: 'rtl'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  addToCart(product: cart) {
    // if(this.cart.addToCart(product) == true) {
    //   this.itemInCart();
    // }
      this.cart.addToCart(product);
      console.log(this.cart.addToCart(product));
      this.presentToast(); 
  }
  addToFav(product: ProductsData) {
    if(this.favs.addToFavourites(product) == true) {
      this.presentToast();
    }
    else {
      this.favs.addToFavourites(product);
      this.itemInFav();
    }
  }
}
