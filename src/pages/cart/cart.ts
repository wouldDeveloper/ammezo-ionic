import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { CartProvider } from '../../providers/cart/cart';
import { Image, ProductsData } from '../../providers/ProductMetaData';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce'
import { product } from '../../providers/CartMetaData'
import { CheckoutPage } from '../checkout/checkout';
import { HomePage } from '../home/home';

 
@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  CartArr:  product[] = [];  
  img: any;
  _productsPrice : string;
  _shippingPrice: string;
  _totalPrice: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private cart: CartProvider, 
    private woo: WoocommerceProvider,
    private load: LoadingController,
    public toast: ToastController,
    ) {


    this.presentLoadingDefault();

    this.getProductsPrice();

    this.getShippingPrice();

    this.getItems();

    this.getTotalPrice();
    console.log(this.getImg);
  }

  ionViewWillEnter() {
    
  }

  ionViewDidLoad() {
    if(this.CartArr.length = 0 ) {
    }
    this.presentLoadingDefault();
    console.log(this.CartArr);
  }
  presentLoadingDefault() {
    let loader = this.load.create({
      spinner: "bubbles",
      content: "جاري التحميل",
      duration: 4000
    });
    loader.present();

    setTimeout(() => {
      this.getItems();
      this.getProductsPrice();
      this.getShippingPrice();
      this.getTotalPrice();
    }, 5000);
  }
  removed() {
    let toast = this.toast.create({
      message: ' تمت الإزالة بنجاح!',
      duration: 3000,
      position: 'top',
      cssClass: 'rtl'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  getItems() {
    this.CartArr = this.cart.getUserCart();
  }
  getImages(id: number) {
    return this.woo.getImg(id);
  }

  getProductsPrice() {
    this._productsPrice = this.cart.productsPrice();
  }

  getShippingPrice() {
    this._shippingPrice = this.cart.shippingPrice();
  }

  getTotalPrice(){
    this._totalPrice = this.cart.totalPrice();
  }

  remove(key: product){
    this.cart.remove(key)
    console.log(this.CartArr);
  }

  getImg(){
     this.img = this.woo.getImg(2728);
     return this.img;
  }

  checkout() {
    this.navCtrl.push(CheckoutPage);
  }
  home() {
    this.navCtrl.push(HomePage);
  }

}
