import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Form } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as Config from '../../config';
import { response } from '../../providers/users';
import { LoginPage } from '../login/login'

import { product } from '../../providers/CartMetaData';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  register_form: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthenticationProvider,
    public formBuilder: FormBuilder
    ) { 
      this.register_form = this.formBuilder.group({
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        displayName: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
      });
    }

    onSubmit(values){
      var username = "root";
      var password = "tillicollapse3";
      //only authenticated administrators can create users
      this.auth.doLogin(username, password)
      .subscribe(
        (res: response) => {
          let user_data = {
            username: values.username,
            name: values.displayName,
            email: values.email,
            password: values.password
          };
          console.log(res.token);
          this.auth.doRegister(user_data, res.token)
          .subscribe(
            result => {
              console.log(result);
            },
            error => {
              console.log(error);
            }
          );
        },
        err => {
          console.log(err);
        }
      )
    }

    login() {
      this.navCtrl.push(LoginPage);
    }
  

}
