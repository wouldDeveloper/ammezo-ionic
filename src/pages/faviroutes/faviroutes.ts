import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FavouritesProvider } from '../../providers/favourites/favourites';
import { Image, ProductsData } from '../../providers/ProductMetaData';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';

@IonicPage()
@Component({
  selector: 'page-faviroutes',
  templateUrl: 'faviroutes.html',
})
export class FaviroutesPage {

  favArr:  ProductsData [] = [];
  img: Array <Image> = [];


  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public fav: FavouritesProvider, 
      private woo: WoocommerceProvider,
      public load: LoadingController,
      public toast: ToastController) {
    this.presentLoadingDefault();
    // this.getImages(this.favArr);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CartPage');
    console.log(this.favArr);
    console.log(this.fav.getFav());
  }
  ionViewWillEnter() {
    this.getItems();
  }
  presentLoadingDefault() {
    let loader = this.load.create({
      spinner: "bubbles",
      content: "جاري التحميل",
      duration: 4000
    });

    loader.present();

    setTimeout(() => {
      this.getItems();
    }, 5000);
  }
  removed() {
    let toast = this.toast.create({
      message: ' تمت الإزالة بنجاح!',
      duration: 3000,
      position: 'top',
      cssClass: 'rtl'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  getItems() {
    this.favArr = this.fav.getFav();
  }
  getImages(id: number) {
    return this.woo.getImg(id);
  }
  remove(item: ProductsData) {
    this.fav.removeItem(item);
    this.removed();
  }
}
