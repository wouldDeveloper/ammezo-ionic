import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FaviroutesPage } from './faviroutes';

@NgModule({
  declarations: [
    FaviroutesPage,
  ],
  imports: [
    IonicPageModule.forChild(FaviroutesPage),
  ],
})
export class FaviroutesPageModule {}
