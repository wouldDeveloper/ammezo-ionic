import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController  } from 'ionic-angular';
import { ProductPage } from '../product/product';
import { ProductsData } from '../../providers/ProductMetaData';
import { WoocommerceProvider } from '../../providers/woocommerce/woocommerce';
import { CartProvider } from '../../providers/cart/cart';



@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {

  products: ProductsData [] = [];
  img: any [];
  idCatePrev: number;
  idCateNext: number;

  constructor(
      public navCtrl: NavController, 
      public navParams: NavParams, 
      public woo: WoocommerceProvider, 
      public load: LoadingController,
      public cart: CartProvider,
      public toast: ToastController) {
        this.getItems();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductsPage');
    console.log(this.navParams.get("category"));
    console.log(this.products);
  }
  ionViewWillLeave() {
    this.idCatePrev = this.navParams.get("category");
  }
  ionViewWillEnter() {
    this.idCateNext = this.navParams.get("category")
  }
  idCompare() {
    if(this.idCateNext === this.idCatePrev) {
      return false;
    }
    else {
       return true;
    }
  }
  getItems() {
    this.reset();
    this.products = this.woo.getProducts(this.navParams.get("category"), this.idCompare());
  }
  goToProduct(id) {
    this.navCtrl.push(ProductPage, { product: id, } );
  }
  reset() {
    if(this.idCompare() == true) {
      this.products.length = 0
    }
  }
  presentToast() {
    let toast = this.toast.create({
      message: 'تمت الإضافة المنتج !',
      duration: 3000,
      position: 'middle'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  itemInCart() {
    let toast = this.toast.create({
      message: ' المنتج موجود داخل السلة!',
      duration: 3000,
      position: 'middle'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }
  // addToCart(product: ProductsData) {
  //   if(this.cart.addToCart(product) == true) {
  //     this.itemInCart();
  //   }
  //   else {
  //     this.cart.addToCart(product);
  //     this.itemInCart();
  //   }  
  // }

}
