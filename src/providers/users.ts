export interface usersData {
    id: number;
    data_created: Date;
    date_created_gmt: Date;
    date_modified: Date;
    date_modified_gmt: Date;
    email: string;
    first_name: string;
    last_name: string;
    role: string;
    username: string;
    password: string;
    billing: billing ;
    shipping: shipping;
    is_paying_customer: boolean;
    orders_count: number;
    total_spent:   string;
    avartar_url: string;
    meta_data: MetaData;


}

interface billing {
    first_name: string;
    last_name: string;
    company: string;
    address_1: string;
    address_2: string;
    city:  string;
    state: string;
    postcode: string;
    country: string;
    email: string;
    phone: string;

}
interface shipping {
    first_name: string;
    last_name: string;
    company: string;
    address_1: string;
    address_2: string;
    city:  string;
    state: string;
    postcode: string;
    country: string;
}
interface MetaData {
    id: number;
    key: string;
    value: string;
}

export interface response {
    token: string;
    user_display_name: string;
    user_email: string;
    user_nicename: string;
}