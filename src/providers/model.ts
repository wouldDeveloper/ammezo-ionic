import { ProductsData } from "./ProductMetaData";

export interface CategoryData {
    id: number;
    name: string;
    slug: string;
    parent: number;
    description: string;
    display: string;
    image: any [];
    menu_order: number;
    count: number
};
