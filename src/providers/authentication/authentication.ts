import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, Component } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import * as Config  from '../../config';
import {  Headers  } from '@angular/http';
import { usersData } from '../users';

@Injectable()
export class AuthenticationProvider {

  user: usersData;

  constructor(
    public http: HttpClient,
    public nativeStorage: NativeStorage,
    ) {
      this.user = localStorage.ammezoAppUser ? JSON.parse(localStorage.ammezoAppUser) : [];
    }

    getUser() {
      return this.user;
    }

    setUser(user) {
      console.log(user);
      this.user = user;
      console.log(this.user);
      return localStorage.ammezoAppUser = JSON.stringify(this.user);
    }

    logOut() {
      this.user = null;
      this.setUser(this.user);
      return this.user;
    }
    isLogged() {
      if(!this.getUser())
        return false;
      else 
        return true;
    }

    doLogin(username, password) {
      return this.http.post(Config.WORDPRESS_URL + 'wp-json/jwt-auth/v1/token', {
        username: username,
        password: password
      });
    }

    doRegister(user_data, token){
      let header : HttpHeaders = new HttpHeaders();
      header.append('Authorization', 'Bearer' + token) 
      return this.http.post(Config.WORDPRESS_REST_API_URL + 'users?token=' + token, user_data, {headers:header});
    }

    validateAuthToken(token){
      let header : HttpHeaders = new HttpHeaders();
      header.append('Authorization','Basic ' + token);
      return this.http.post(Config.WORDPRESS_URL + 'wp-json/jwt-auth/v1/token/validate?token=' + token,
        {}, {headers: header})
    }
} 
