import { Injectable } from '@angular/core';
import { Image, ProductsData } from '../ProductMetaData';


@Injectable()
export class FavouritesProvider {

  products:  ProductsData [] = [];
  img: Array <Image> = [];

  constructor() {
    console.log('Hello FavouritesProvider Provider');
    this.products = localStorage.ammezoAppFav ? JSON.parse(localStorage.ammezoAppFav) : [];
  }

  addToFavourites(item: ProductsData ) {
    if (this.products.findIndex(x => x.id == item.id) == -1) {
      item.quantity = 1;
      this.products.push(item);
      this.saveFav();
      console.log(this.products);
    }
    else {
      return true;
    }
  }
  
  saveFav() {
   localStorage.ammezoAppFav = JSON.stringify(this.products);
  }
  getFav(): Array <ProductsData> {
    return this.products;
  }
  removeItem(item: ProductsData) {
  var index = this.products.indexOf(item)
  if(index > -1) {
    this.products.splice(index, 1);
  }
  }

}
