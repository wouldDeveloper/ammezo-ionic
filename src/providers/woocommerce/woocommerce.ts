import { Injectable } from '@angular/core';
import { WooApiService } from 'ng2woo';
import { CategoryData } from '../model';
import * as Config  from '../../config';
import { ProductsData, Image } from '../ProductMetaData';

@Injectable()
export class WoocommerceProvider {

categoires: CategoryData[] = [];
products: ProductsData [] = [];
results: ProductsData [] = [];
product: ProductsData = <ProductsData>{};
img: Image = <Image>{};

  constructor( public woo: WooApiService) {
  
    console.log('Hello WoocommerceProvider Provider');
  }
  getCate()  {
    this.woo.fetchItems('products/categories?per_page=100')
    .then((categoires:CategoryData[] = []) => categoires.forEach(element => {
      this.categoires.push(element);
  }))
  .catch(error => console.log(error));
  return this.categoires;
  }

  getProducts(id: number, same: boolean) {
    this.woo.fetchItems('products?category=' + id)
    .then((products: ProductsData [] = []) => 
      { products.forEach(element => {
          if(same == false) {
            this.products.length = 0;
          }
          if(this.products.findIndex(x => x.id == element.id) == -1) {
            this.products.push(element);
          }
        }) 
    })
    .catch(error => console.log(error))
    return this.products;
  }

  getProduct(id: number) {
     this.woo.fetchItems('products/' + id)
     .then(( product: ProductsData = <ProductsData>{} )  => this.product = product)
     .catch(error => console.log(error));
     return this.product;
  }

  getImg(id: number) {
    this.woo.fetchItems('products/' + id + '/variations/' + id)
    .then(product => this.img = product.image)
    .catch(error => console.log(error));
    return this.img;
  }
  
  searchProducts(token: string) {
    this.woo.fetchItems('products/?search=' + token)
    .then(products => 
      { products.forEach(element => {
        if(token != "") {
          if(this.results.findIndex(x => x.id == element.id) == -1) {
            this.results.push(element)
          }
        }
        else {
          return this.results.length = 0;
        }
      })
    })
    .catch(error => console.log(error));
    return this.results;
  }

  getUsers() {
    this.woo.fetchItems('customers')
    .then(customers => console.log(customers))
    .catch(error => console.log(error))
  }
  getUserCart() {
    this.woo.fetchItems('cart')
    .then(cart => console.log(cart))
    .catch(error => console.log(error))
  }
}
