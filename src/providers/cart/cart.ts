import { Injectable } from '@angular/core';
import { Image, ProductsData, cart } from '../ProductMetaData';
import { WooApiService } from 'ng2woo';
import { HttpClient} from '@angular/common/http';
import { RequestOptions } from '@angular/http';
import {  Response, Headers } from '@angular/http';
import * as Config  from '../../config';
import { price, product } from '../CartMetaData';
import {Observable} from 'rxjs/Rx';


@Injectable()
export class CartProvider {

	img: Array <Image> = [];
	_productsPrice: string;
	_totalPrice: string;
	inCartItems: product[] = [];
	_shippingPrice: string;
	item: string;
  	constructor(public woo: WooApiService, public http: HttpClient) { }
	
	addToCart(item): Observable<ProductsData[]> {
		let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
		let options      = new RequestOptions({ headers: headers }); // Create a request option

		return this.http.post(Config.WORDPRESS_URL + 'wp-json/wc/v2/cart/add', item)
		.map((res : Response) => res.json())
		.catch((error: any) => Observable.throw(error.json().error || 'server error'))
	}

	getUserCart() {
		this.woo.fetchItems('cart')
		.then((cart: product) => {
			this.inCartItems = Object.keys(cart).map(i => cart[i]);
		})
		.catch(error => console.log(error))
		console.log(this.inCartItems);
		return this.inCartItems;
	  }

	productsPrice():string {
		this.woo.fetchItems('cart/totals')
		.then((price:price) => this._productsPrice = price.subtotal)
		.catch(error=> console.log(error))
		return this._productsPrice;
	}
	totalPrice():string {
		this.woo.fetchItems('cart/totals')
		.then((price: price) => this._totalPrice = price.total)
		.catch(err => console.log(err))
		return this._totalPrice;
	}
	shippingPrice():string {
		this.woo.fetchItems('cart/totals')
		.then((price:price) => this._shippingPrice = price.shipping_total)
		.catch(err => console.log(err))
		return this._shippingPrice;
	}
	remove(key:product) {
		this.woo.fetchItems('cart/' + key + '/' + 2728) 
		.then((pro:product) => console.log(pro))
		.catch(err => console.log(err))
		return this.item;
		
	}

	testingPrice() {
		console.log(this.getUserCart());
	}
	
	  
}
