interface DownloadsPro {
    id: string;
    name: string;
    file: string;
}
interface DimensionsPro {
    length: string;
    width: string;
    height: string;
}
interface CatePro {
    id: number;
    name: string;
    slug: string;
}
interface TagsPro {
    id: number;
    name: string;
    slug: string;
}
interface AttributesPro {
    id: number;
    name: string;
    position: number;
    visible: boolean;
    variation: boolean;
    options: Array<any>;
}
interface AttributesProDef {
    id: number;
    name: string;
    option: string;
}
interface MetaData {
    id: number;
    key: string;
    value: string;
}
export interface cart {
    product_id: number;
    quantity: number;
    variation_id: number;
    variation: number;
    cart_item_data: number;
}
 export interface Image {
    alt: string;
    date_created: Date;
    date_created_gmt: Date;
    date_modified: Date;
    date_modified_gmt: Date;
    id: number;
    src: string;
    name: string;
    position: number;
} 
 export interface ProductsData {
    id:number;
    image: Image;
    name: string;
    slug: string;
    permalink: string;
    date_created: Date;
    data_created_gmt: Date;
    date_modified: Date;
    date_modified_gmt: Date;
    type: string;
    status: string;
    featured: boolean;
    catalog_visibility: string;
    description	: string;
    short_description: string;
    sku: string;
    price: string;
    regular_price: string;
    sale_price: string;
    date_on_sale_from: Date;
    date_on_sale_from_gmt: Date;
    date_on_sale_to: Date;
    date_on_sale_to_gmt: Date;
    price_html: string;
    on_sale: boolean;
    purchasable: boolean;
    total_sales: number;
    virtual: boolean;
    downloadable: boolean;
    downloads: DownloadsPro [];
    download_limit: number;
    download_expiry: number;
    external_url: string;
    button_text: string;
    tax_status: string;
    tax_class: string;
    manage_stock: boolean;
    quantity: number;
    stock_quantity: number;
    in_stock: boolean;
    backorders: string;
    backorders_allowed: boolean;
    backordered: boolean;
    sold_individually: boolean;
    weight: string;
    dimensions: DimensionsPro;
    shipping_required: boolean;
    shipping_taxable: boolean;
    shipping_class: string;
    shipping_class_id: number;
    average_rating: string;
    rating_count: number;
    related_ids: Array <any>;
    upsell_ids: Array <any>;
    cross_sell_ids: Array <any>;
    parent_id: number;
    purchase_note: string;
    categories: CatePro[];
    tags: TagsPro [];
    attributes: AttributesPro[];
    default_attributes: AttributesProDef [];
    variations: Array<any>;
    grouped_products:  Array<any>;
    menu_order: number;
    meta_data: MetaData[];
}