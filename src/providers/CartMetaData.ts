
export interface price {
    cart_contents_tax: number;
    cart_contents_taxes: Object[];
    cart_contents_total: string;
    discount_tax: number;
    discount_total: number;
    fee_tax: number;
    fee_taxes: Object[];
    fee_total: string;
    shipping_tax: number;
    shipping_taxes: Object[];
    shipping_total: string;
    subtotal: string;
    subtotal_tax: number;
    total: string;
    total_tax: number;
}
export interface product {
    data: Object;
    data_hash: string;   
    key: string;
    line_subtotal: number;
    line_subtotal_tax: number;
    line_tax: number;
    line_tax_data: Object
    line_total: number;
    product_id: number;
    product_name: string;
    quantity: number;
    variation: Array<any>;
    variation_id: number;
}