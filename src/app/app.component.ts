import { Component, ViewChild } from '@angular/core';
import {  Platform,NavController, MenuController, LoadingController, Tab } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { HomePage } from '../pages/home/home';
import { CacheService } from "ionic-cache";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  account = LoginPage;

  @ViewChild('nav') nav: NavController;  

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    private menuCont: MenuController,
    public auth: AuthenticationProvider,
    public loading: LoadingController,
    cache: CacheService) {
      platform.ready().then(() => {
        cache.setDefaultTTL(60 * 60 * 12);
        cache.setOfflineInvalidate(false);

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });  
  }

  logOut() {
    let loading = this.loading.create();
    loading.present();
    this.auth.logOut();
    this.nav.setRoot(TabsPage);
    this.menuCont.close();
    loading.dismiss();
  }

  isLogged() {
    return this.auth.isLogged();
  }

  onLoad(page: any) {
    this.nav.setRoot(page);
    this.menuCont.close()
  }
  goHome() {
    this.menuCont.close()
  }

}
