import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { CacheModule } from 'ionic-cache';

import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HeaderAmmComponent } from '../components/header-amm/header-amm';
import { CategoriesPage } from '../pages/categories/categories'
import { ProductsPage } from '../pages/products/products';
import { CheckoutPage } from '../pages/checkout/checkout'

import { RateComponent } from '../components/rate/rate';
import { ProductPage } from '../pages/product/product';
import { CartPage } from '../pages/cart/cart';

import { FaviroutesPage } from '../pages/faviroutes/faviroutes';
import { WoocommerceProvider } from '../providers/woocommerce/woocommerce';
import { WooApiModule, WooApiService } from 'ng2woo';

import { CartProvider } from '../providers/cart/cart';
import { NativeStorage } from '@ionic-native/native-storage';
import { FavouritesProvider } from '../providers/favourites/favourites';

import { SearchModalPage } from '../pages/search-modal/search-modal';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';

import { AccountPage } from '../pages/account/account';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';



export const WooCommerceConfig = {
  url: 'http://ammezo.com', // Your store URL
  consumerKey: 'ck_3c449d10c41e3bec3cabe73220ded438e5252769', // Your consumer key
  consumerSecret: 'cs_bf82a85bcb65b7de8f1eb5b170f009103368b0c8', // Your consumer secret
  wpAPI: true, // Enable the WP REST API integration
  version: 'wc/v2' // WooCommerce WP REST API version
}


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    HeaderAmmComponent,
    CategoriesPage,
    ProductsPage,
    RateComponent,
    ProductPage,
    CartPage,
    FaviroutesPage,
    SearchModalPage,
    RegisterPage,
    LoginPage,
    AccountPage,
    CheckoutPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    // Specify as an import and pass in your config
    WooApiModule.forRoot(WooCommerceConfig),
    HttpClientModule,
    CacheModule.forRoot()


    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    HeaderAmmComponent,
    CategoriesPage,
    ProductsPage,
    RateComponent,
    ProductPage,
    CartPage,
    FaviroutesPage,
    SearchModalPage,
    RegisterPage,
    LoginPage,
    AccountPage,
    CheckoutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WoocommerceProvider,
    WooApiService,
    CartProvider,
    NativeStorage,
    FavouritesProvider,
    // HttpHeaders,
    AuthenticationProvider,

  ]
})
export class AppModule {}
