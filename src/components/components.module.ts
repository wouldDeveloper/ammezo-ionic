import { NgModule } from '@angular/core';
import { HeaderAmmComponent } from './header-amm/header-amm';
import { RateComponent } from './rate/rate';
import { ProductComponent } from './product/product';
@NgModule({
	declarations: [HeaderAmmComponent,
    RateComponent,
    ProductComponent],
	imports: [],
	exports: [HeaderAmmComponent,
    RateComponent,
    ProductComponent]
})
export class ComponentsModule {}
