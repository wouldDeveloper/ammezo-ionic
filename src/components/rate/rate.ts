import { Input, EventEmitter ,Output} from "@angular/core";
import { Component } from '@angular/core';



enum COLORS  {
  grey = "#E0E0E0",
  green = "#fcb800",
  YELLO = "#fcb800",
  RED = "#fcb800"
}

@Component({
  selector: 'rate',
  templateUrl: 'rate.html'
})

export class RateComponent {

  @Input() rating: number ;

  @Output() ratingChange: EventEmitter<number> = new EventEmitter();;


  text: string;

  constructor() {
    console.log('Hello RateComponent Component');
    this.text = 'Hello World';
  }
  rate(index: number) {
    this.rating = index;
    this.ratingChange.emit(this.rating);
 }

 getColor(index: number) {
    if(this.isAboveRating(index)){
      return COLORS.grey;
    }
    switch (this.rating) {
      case 1:
      case 2:
        return COLORS.RED;
      case 3:
        return COLORS.YELLO;
      case 4:
      case 5:
        return COLORS.green;
      default:
        return COLORS.grey;
    }
}

isAboveRating(index: number = 5): boolean {
  return index > this.rating;
}

}
