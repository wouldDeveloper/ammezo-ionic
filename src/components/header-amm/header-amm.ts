import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CartPage } from '../../pages/cart/cart';
import { MenuController } from 'ionic-angular';
import { SearchModalPage } from '../../pages/search-modal/search-modal';


@Component({
  selector: 'header-amm',
  templateUrl: 'header-amm.html'
})
export class HeaderAmmComponent {

  text: string;

  constructor(private menuCont:  MenuController, private navCrtl: NavController) {
    console.log('Hello HeaderAmmComponent Component');
    this.text = 'Hello World';
  }

  openMenu() {
      this.menuCont.toggle()
    }
  goCart() {
    this.navCrtl.push(CartPage);
  }

  goSearch() {
    this.navCrtl.push(SearchModalPage);
  }

}
